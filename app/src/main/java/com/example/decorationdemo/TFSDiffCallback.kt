package com.example.decorationdemo

import androidx.recyclerview.widget.DiffUtil

/**
 * @author d.kleshchev
 */
class TFSDiffCallback : DiffUtil.ItemCallback<Post>() {

    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.subtitle == newItem.subtitle
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: Post, newItem: Post): Any? {
        return newItem.title
    }
}
