package com.example.decorationdemo

import android.graphics.Canvas
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_divider.view.*

/**
 * @author d.kleshchev
 */
class TFSDecorator : RecyclerView.ItemDecoration() {

    private var dateDecor: View? = null

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val adapter = parent.adapter
        if (adapter is DecorationTypeProvider) {
            parent.children.forEach { child ->
                val childAdapterPosition = parent.getChildAdapterPosition(child)

                val type = adapter.getType(childAdapterPosition)

                if (type is DecorationType.WithText) {
                    val dateView = getDateDecor(parent)
                    dateView.dividerText.text = type.text

                    c.save()
                    c.translate(0f, (child.top - dateView.height).toFloat())
                    dateView.draw(c)
                    c.restore()
                }
            }
        } else {
            super.onDraw(c, parent, state)
        }
    }

    // draw children

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)

        val topChild = parent.getChildAt(0) ?: return

        val childAdapterPosition = parent.getChildAdapterPosition(topChild)

        if (childAdapterPosition == RecyclerView.NO_POSITION) {
            return
        }
        val adapter = parent.adapter
        if (adapter is DecorationTypeProvider) {
            val type = adapter.getType(0)

            if (type is DecorationType.WithText) {
                val dateView = getDateDecor(parent)
                dateView.dividerText.text = type.text

                c.save()
                dateView.draw(c)
                c.restore()
            }
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        if (position == RecyclerView.NO_POSITION) {
            return
        }

        val adapter = parent.adapter
        if (adapter is DecorationTypeProvider) {
            val type = adapter.getType(position)


            outRect.top = if (type is DecorationType.Space) {
                OFFSET.dpToPx(view.context).toInt()
            } else {
                getDateDecor(parent).height
            }
        }
    }

    private fun getDateDecor(parent: ViewGroup): View {
        if (dateDecor == null) {
            dateDecor = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_divider, parent, false)

            dateDecor!! fixLayoutSizeIn parent
        }

        return dateDecor!!
    }

    private infix fun View.fixLayoutSizeIn(parent: ViewGroup) {
        if (layoutParams == null) {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        val widthSpec =
            View.MeasureSpec.makeMeasureSpec(parent.width, View.MeasureSpec.EXACTLY)

        val heightSpec =
            View.MeasureSpec.makeMeasureSpec(parent.height, View.MeasureSpec.UNSPECIFIED)

        val childWidth = ViewGroup.getChildMeasureSpec(
            widthSpec,
            parent.paddingLeft + parent.paddingRight,
            layoutParams.width
        )

        val childHeight = ViewGroup.getChildMeasureSpec(
            heightSpec,
            parent.paddingTop + parent.paddingBottom,
            layoutParams.height
        )

        measure(childWidth, childHeight)
        layout(0, 0, measuredWidth, measuredHeight)
    }

    private companion object {
        const val OFFSET = 8
    }
}
