package com.example.decorationdemo

/**
 * @author d.kleshchev
 */
interface DecorationTypeProvider {

    fun getType(position: Int): DecorationType
}
