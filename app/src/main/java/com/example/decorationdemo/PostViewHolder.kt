package com.example.decorationdemo

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_post.view.*

/**
 * @author d.kleshchev
 */
class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(post: Post) {
        itemView.title.text = post.title
        itemView.subtitle.text = post.subtitle
    }
}
