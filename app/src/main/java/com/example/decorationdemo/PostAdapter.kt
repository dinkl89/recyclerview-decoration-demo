package com.example.decorationdemo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.joda.time.format.DateTimeFormat

/**
 * @author d.kleshchev
 */
class PostAdapter(
    val swipeListener: (String) -> Unit
) : RecyclerView.Adapter<PostViewHolder>(),
    DecorationTypeProvider,
    SwipeListener {

    private val dateFormat = DateTimeFormat.forPattern("dd MMMM")

    var items: MutableList<Post> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getType(position: Int): DecorationType {
        if (position == RecyclerView.NO_POSITION) {
            return DecorationType.Space
        }

        if (items.isEmpty()) {
            return DecorationType.Space
        }

        if (position == 0) {
            return DecorationType.WithText(items[0].date.toString(dateFormat))
        }

        val current = items[position]
        val previous = items[position - 1]

        return if (current.date.dayOfYear().get() == previous.date.dayOfYear().get()) {
            DecorationType.Space
        } else {
            DecorationType.WithText(items[position].date.toString(dateFormat))
        }
    }

    override fun onSwipe(position: Int) {
//        swipeListener("Item #$position swiped")
//        notifyItemChanged(position)

        items.removeAt(position)
        notifyItemRemoved(position)
    }
}
