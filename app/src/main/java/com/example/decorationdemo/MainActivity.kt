package com.example.decorationdemo

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.joda.time.DateTime

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val items = getItems()

        val postAdapter = PostAdapter {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create()
                .show()
        }
        postAdapter.items = items

        val callback = TFSTouchHelperCallback(postAdapter)
        val touchHelper = ItemTouchHelper(callback)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = postAdapter
        recycler.addItemDecoration(TFSDecorator())
        touchHelper.attachToRecyclerView(recycler)
    }

    private fun getItems(): MutableList<Post> {
        var date = DateTime.now()
        return (0..10).map {

            if (it % 2 == 0) {
                date = date.plusDays(it)
            }

            Post(
                title = "Шок! Разрыв шаблонов $it",
                subtitle = "Чтобы написать $it крутых декораций, нужно всего лишь...",
                date = date
            )
        }
            .toMutableList()
    }
}