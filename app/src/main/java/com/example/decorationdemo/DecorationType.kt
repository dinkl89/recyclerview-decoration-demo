package com.example.decorationdemo

/**
 * @author d.kleshchev
 */
sealed class DecorationType {

    object Space : DecorationType()

    class WithText(
        val text: String
    ) : DecorationType()
}
