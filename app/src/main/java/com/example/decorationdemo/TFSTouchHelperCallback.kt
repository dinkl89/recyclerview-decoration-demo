package com.example.decorationdemo

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs

/**
 * @author d.kleshchev
 */
class TFSTouchHelperCallback(
    private val swipeListener: SwipeListener
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {

    private val paint = Paint()

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        swipeListener.onSwipe(viewHolder.adapterPosition)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val itemView = viewHolder.itemView
            val translationX = abs(dX)

            itemView.translationX = translationX

            paint.color = Color.CYAN
            val rectF = RectF(
                itemView.left + translationX,
                itemView.top.toFloat(),
                itemView.left.toFloat(),
                itemView.bottom.toFloat(),
            )

            c.drawRect(rectF, paint)
        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }
}
