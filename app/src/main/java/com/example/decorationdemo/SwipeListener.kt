package com.example.decorationdemo

/**
 * @author d.kleshchev
 */
interface SwipeListener {

    fun onSwipe(position: Int)
}
