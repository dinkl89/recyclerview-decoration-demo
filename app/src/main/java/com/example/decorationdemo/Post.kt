package com.example.decorationdemo

import org.joda.time.DateTime

/**
 * @author d.kleshchev
 */
data class Post(
    val title: String,
    val subtitle: String,
    val date: DateTime
)
